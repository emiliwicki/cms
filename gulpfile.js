var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');
var webpack_stream = require('webpack-stream');
var webpack_config = require('./webpack/webpack.config.js');

var plugins = gulpLoadPlugins({
  pattern: '*',
  scope: ['dependencies', 'devDependencies']
});

var path = {
  src: './resources/assets/',
  dev: './public/',
  admin: './public/assets/admin/',
  project: './public/assets/project/'
};

var config = {
    js: {
      src: path.src + 'js/main.js',
      webpack: webpack_config,
      dev: path.dev + 'js'
    },
    sass: {
      src: path.src + 'scss/main.scss',
      dev: path.dev + 'css'
    },
    sass_tiny: {
      src: path.src + 'sass/tiny_custom.scss',
      project: path.project + 'css'
    },
    // css: {
    //   src: path.core.src + 'css/**/*.css',
    //   destDev: path.core.dev + 'css',
    //   destProd: path.prod + 'css'
    // },
    img: {
      src: path.src + 'img/**/*.*',
      dev: path.dev + 'img'
    },
    fonts: {
      src: path.src + 'fonts/*.*',
      dev: path.dev + 'fonts'
    }
};

function allocationOfTasks(task, taskName) {
  switch (task) {
    case 'js':
      jsFunc(taskName['src'], taskName['webpack'], taskName['dev']);
      break;
    case 'sass':
      sassFunc(taskName['src'], taskName['dev']);
      break;
    case 'sass_tiny':
      sassTinyFunc(taskName['src'], taskName['project']);
      break;
    // case 'css':
    //   cssFunc(taskName['src'], taskName['destDev'], taskName['destProd']);
    //   break;
    case 'img':
      imgFunc(taskName['src'], taskName['dev']);
      break;
    case 'fonts':
      fontsFunc(taskName['src'], taskName['dev']);
      break;
  }
}

function jsFunc(src, webpack, dev) {
  gulp.src(src)
    .pipe(webpack_stream(webpack))
    .pipe(gulp.dest(dev))
}

function sassFunc(src, dev) {
  gulp.src(src)
    .pipe(plugins.sass())
    .pipe(plugins.rename('main_1.0.css'))
    .pipe(gulp.dest(dev))
}

function sassTinyFunc(src, project) {
  gulp.src(src)
    .pipe(plugins.sass())
    .pipe(plugins.rename('tiny_1.0.css'))
    .pipe(gulp.dest(project))
}

function imgFunc(src, dev) {
  gulp.src(src)
    .pipe(gulp.dest(dev))
}

function fontsFunc(src, dev) {
  gulp.src(src)
    .pipe(gulp.dest(dev))
}

function cssFunc(src, destDev, destProd) {
  gulp.src(src)
    .pipe(gulp.dest(destDev))
    .pipe(gulp.dest(destProd))
}

gulp.task('default', function () {
      for (var task in config) {
        var taskName = config[task];

        allocationOfTasks(task, taskName);
      }

      plugins.util.log(plugins.util.colors.blue('SCSS watching...'));

      gulp.watch(path.src + 'scss/**/*.scss').on('change', function(){
        sassFunc('./resources/assets/scss/main.scss', './public/css');
        sassTinyFunc(path.src + 'scss/tiny_custom.scss', path.project + 'css');
        plugins.util.log(plugins.util.colors.green('SCSS changed...'));
      });
});
