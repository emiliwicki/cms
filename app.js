var express = require('express');
var bodyParser = require('body-parser');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose')
var sessions = require('client-sessions');
var bcrypt = require('bcryptjs');


var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

mongoose.connect('mongodb://localhost/cms');

var User = mongoose.model('User', new Schema({
  id: ObjectId,
  login: { type: String, unique: true },
  password: String,
  email: String
}));

var app = express();
app.set('view engine', 'pug');
app.use('/assets', express.static('public'));

/* MIDDLEWARE = use() */

app.use(bodyParser.urlencoded({ extended: true }));

app.use(sessions({
  cookieName: 'session',
  secret: 'dsfsdfsifojw9483h5h39j94g3n84yg83y423',
  duration: 30 * 60 * 1000,
  activeDuration: 5 * 60 * 1000
}));

app.post('/register', function(req, res){
    var hash = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));

    var user = new User({
      login: req.body.login,
      password: hash,
      email: req.body.email
    });

    user.save(function(err){
      if(err){
        console.log(err);
      }else{
        res.redirect('/dashboard');
      }
    });
});


app.get('/', function(req, res){
    res.render('base', { header: 'Witaj na stronie main!'});
});

app.get('/login', function(req, res){
    res.render('sites/login_panel');
});

app.post('/login', function (req, res) {
  User.findOne({ login: req.body.login }, function(err, user){
    if(!user){
      console.log('user not exist!');
    }else{
      if(bcrypt.compareSync(req.body.password, user.password)){
        req.session.user = user;
        res.redirect('/dashboard');
      }else{
        console.log('wrong Password');
        res.render('sites/login_panel', { error: 'Invalid password'});
      }
    }
  });
});

app.get('/register', function(req, res){
    res.render('sites/register_panel');
});

app.get('/dashboard', function(req, res){
    if(req.session && req.session.user){
      User.findOne({ login: req.session.user.login}, function(err, user){
          if(!user){
            req.session.reset();
            res.redirect('/login');
          }else{
            res.locals.user = user;
            res.render('sites/dashboard');
          }
      });
    }else{
      res.render('sites/dashboard');
    }
});

app.get('/logout', function(req, res){
    req.session.reset();
    res.redirect('/');
});

app.listen(3000);
